;
(function(mangoFunction) {
	function headerNav(el) {
		this.init(el);
	}
	headerNav.prototype = {
		init(el) {
			this.$el = $(el);
			this.$nav = this.$el.find('nav');
			this.$menu = this.$el.find('[nav-menu]');
			this.isOpen = false;
			this.setNav();
		},
		setNav() {
			this.$nav.click(() => {
				if(this.isOpen) {
					this.$nav.addClass('active')
					this.$menu.addClass('active')
				} else {
					this.$nav.removeClass('active')
					this.$menu.removeClass('active')
				}
				this.isOpen = !this.isOpen;
			})
		}
	}

	function homeMap(el) {
		this.init(el);
	}
	homeMap.prototype = {
		init(el) {
			this.$el = $(el);
			this.$mapContainer = this.$el.find('[map-container]');
			this.$mapList = this.$el.find('[list-area]');
			this.mapSvg = mangoFunction.getMapSVG();
			this.generateMap();
		},
		generateMap() {
			this.$mapContainer.html(this.mapSvg.map);
			this.$mapArea = this.$mapContainer.find('[map-area]');
			this.$mapArea.each((index, item) => {
				let $item = $(item);
				$item.hover(() => {
					this.$mapArea.removeClass('active');
					this.$mapList.removeClass('active');
					$item.addClass('active');
					this.$el.find('[list-area="' + $item.attr('map-area') + '"]').addClass('active');
				})
			})
			this.$mapList.each((index, item) => {
				let $item = $(item);
				$item.hover(() => {
					this.$mapArea.removeClass('active');
					this.$mapList.removeClass('active');
					$item.addClass('active');
					this.$el.find('[map-area="' + $item.attr('list-area') + '"]').addClass('active');
				})
			})
		}
	}

	function thunderSlider(el) {
		return new Promise((r, l) => {
			this.init(el);
			r(this);
		})
	}
	thunderSlider.prototype = {
		init(el) {
			this.$el = $(el);
			this.$list = this.$el.find('[thunder-list] li');
			this.$insert = this.$el.find('[thunder-insert]');
			this.$insert.html(this.$list.clone());
			this.$swiper = new Swiper(this.$el.find('.swiper-container'), {
				speed: 400,
				navigation: {
					nextEl: '.navigationFlex .next',
					prevEl: '.navigationFlex .prev',
				},
			});
			this.setBtn();
		},
		setBtn() {
			this.$list.each((index, item) => {
				let $item = $(item);
				$item.hover(() => {
					this.$swiper.slideTo(index);
				})
			})
		}
	}

	function tabs(el) {
		this.init(el);
	}
	tabs.prototype = {
		init(el) {
			this.$el = $(el);
			this.$link = this.$el.find('[link-block] li');
			this.$content = this.$el.find('[content-block] li');
			this.setBtn();
			this.goTab(1);
		},
		setBtn() {
			this.$link.each((index, item) => {
				let $item = $(item);
				$item.click(() => {
					this.goTab(index);
				})
			})
		},
		goTab(num) {
			this.$link.removeClass('active');
			this.$link.eq(num).addClass('active');
			this.$content.hide();
			this.$content.eq(num).show();
		}
	}

	function shellList(el) {
		this.init(el);
	}
	shellList.prototype = {
		init(el) {
			this.$el = $(el);
			this.$list = this.$el.find('li');
			this.setBtn();
		},
		setBtn() {
			this.$list.each((index, item) => {
				let $item = $(item);
				let initHeight = $item.find('h3').outerHeight();
				let extandHeight = $item.find('p').outerHeight() + initHeight;
				$item.find('h3').click(() => {
					if($item.hasClass('active')) {
						$item.removeClass('active');
						$item.height(initHeight);
					} else {
						$item.addClass('active');
						$item.height(extandHeight);
					}
				})
				$item.height(initHeight);
			})
		}
	}

	function cooperatePage(el, data) {
		this.init(el, data);
	}
	cooperatePage.prototype = {
		init(el, data) {
			this.$el = $(el);
			this.Data = data;
			this.$brandInsert = this.$el.find('[brand-insert]');
			this.$stickyInsert = this.$el.find('[sticky-insert]');
			this.template = {
				$brand: this.$el.find('template[brand]'),
				$sticky: this.$el.find('template[sticky]'),
				$slide: this.$el.find('template[slide]'),
			}
			this.generateWithData();
		},
		generateWithData() {
			this.$brandInsert.html('');
			let i = 0;
			for(let d of this.Data) {
				this.$brandInsert.append(this.generateBrand(d, i))
				i++;
			}
			if(this.Data.length > 0) {
				this.generateSticky(0)
			}
		},
		generateBrand(d, i) {
			let $tempBrand = $(this.template.$brand.html());
			$tempBrand.find('div').css('background-image', 'url(' + d.logo + ')');
			$tempBrand.find('h3').html(d.ch);
			$tempBrand.find('p').html(d.en);
			$tempBrand.click(() => {
				this.generateSticky(i);
			})
			return $tempBrand;
		},
		generateSticky(i) {
			this.$stickyInsert.html('');
			let $tempSticky = $(this.template.$sticky.html());
			let $swiperInsert = $tempSticky.find('[swiper-insert]');
			for(let img of this.Data[i].img) {
				$swiperInsert.append(this.generateSlide(img));
			}
			$tempSticky.find('p').html(this.Data[i].des);
			this.$stickyInsert.html($tempSticky);
			new Swiper($tempSticky.find('.swiper-container'), {
				autoplay: true,
				pagination: {
					el: $tempSticky.find('.swiper-pagination'),
					type: 'bullets',
				},
			});
		},
		generateSlide(img) {
			let $tempSlide = $(this.template.$slide.html());
			$tempSlide.find('div').css('background-image', 'url(' + img + ')');
			return $tempSlide;
		}
	}

	function mapContent(el, data) {
		this.init(el, data);
	}
	mapContent.prototype={
		init(el,data){
			this.$el=$(el);
			this.Data = data;
			this.$coverInsert=this.$el.find('[cover-insert]');
			this.$sliderInsert=this.$el.find('[slider-insert]');
			this.$detecBox = this.$sliderInsert;
			this.isOpen = false;
			this.mainSwiper = null;
			this.thumbSwiper = null;
			this.template={
				$sliderBox:this.$el.find('template[slider-box]'),
				$coverImg:this.$el.find('template[cover-img]'),
				$mainSlide:this.$el.find('template[main-slide]'),
				$thumbSlide:this.$el.find('template[thumb-slide]'),
				$thumbSlideInside:this.$el.find('template[thumb-slide-inside]'),
			}
			this.$sliderInsert.html('');
			this.generateWithData();
			this.setBtn()
		},
		setBtn(){
			$(window).click((e)=>{
				if(this.isOpen&&this.$detecBox.find($(e.target)).length==0){
					this.close();
				}
			})
		},
		generateWithData(data){
			this.$coverInsert.html('');
			for(let d of this.Data){
				this.$coverInsert.append(this.generateCoverImg(d));
			}
		},
		generateCoverImg(d){
			let $tmpCoverImg = $(this.template.$coverImg.html());
			let $img = $tmpCoverImg.find('img');
			$img.attr('src',d.coverImg);
			$tmpCoverImg.find('p').html(d.name);
			$tmpCoverImg.click(()=>{
				this.generateSlideBox(d);
			})
			return $tmpCoverImg;
		},
		generateSlideBox(d){
			this.$sliderInsert.html('');
			let $tmpSliderBox = $(this.template.$sliderBox.html());
			let $mainSlideInsert = $tmpSliderBox.find('[main-slide-insert]');
			let $thumbSlideInsert = $tmpSliderBox.find('[thumb-slide-insert]');
			let thumbArray = [[]];
			let i = 0;
			for(let img of d.img){
				if(thumbArray[thumbArray.length-1].length<2){
					thumbArray[thumbArray.length-1].push({img,index:i});
				}else{
					thumbArray.push([]);
					thumbArray[thumbArray.length-1].push({img,index:i});
				}
				i++;
				$mainSlideInsert.append(this.generateMainSlide(img));
			}
			for(let thumbCombo of thumbArray){
				$thumbSlideInsert.append(this.generateThumbCombo(thumbCombo));
			}
			this.$sliderInsert.html($tmpSliderBox);
			this.mainSwiper = new Swiper($tmpSliderBox.find('[main-swiper]'),{});
			this.thumbSwiper = new Swiper($tmpSliderBox.find('[thumb-swiper]'),{slidesPerView:5,});
			$tmpSliderBox.find('[content-insert] h3').html(d.name);
			$tmpSliderBox.find('[content-insert] p').html(d.content);
			// mangoFunction.SetPhotofix($tmpSliderBox.find('.photofix'));
			this.$detecBox = this.$el.find('[detect-box]');
			this.open();
		},
		generateThumbCombo(thumbCombo){
			let $tmpThumbSlide = $(this.template.$thumbSlide.html());
			let $thumbSlideInsideInsert = $tmpThumbSlide;
			for(let t of thumbCombo){
				$thumbSlideInsideInsert.append(this.generateThumbSlideInside(t))
			}
			return $thumbSlideInsideInsert;
		},
		generateThumbSlideInside(t){
			let $thumbSlideInside = $(this.template.$thumbSlideInside.html());
			let $img = $thumbSlideInside.find('img')
			$img.on('load',()=>{
				mangoFunction.SetPhotofix('.photofix');
			})
			$img.attr('src',t.img);
			$thumbSlideInside.click(()=>{
				this.mainSwiper.slideTo(t.index);
			})
			return $thumbSlideInside;
		},
		generateMainSlide(img){
			let $mainSlideTmp = $(this.template.$mainSlide.html());
			$mainSlideTmp.find('div').css('background-image','url('+img+')');
			return $mainSlideTmp;
		},
		open(){
			this.$sliderInsert.addClass('active');
			setTimeout(()=>{this.isOpen = true;},200);
		},
		close(){
			this.$sliderInsert.removeClass('active');
			setTimeout(()=>{this.isOpen = false;},200);
		}
	}

	mangoFunction.homeMap = function(el = '#homeMap') {
		return new homeMap(el)
	}
	mangoFunction.headerNav = function(el = 'header') {
		return new headerNav(el)
	}
	mangoFunction.thunderSlider = function(el = "#thunderSlider") {
		return new thunderSlider(el)
	}
	mangoFunction.tabs = function(el = "#tabs") {
		return new tabs(el)
	}
	mangoFunction.shellList = function(el = "#shellList") {
		return new shellList(el)
	}
	mangoFunction.mapContent = function(el = "#mapContent", data = []) {
		return new mapContent(el, data)
	}
	mangoFunction.cooperatePage = function(el = "#cooperatePage", data = []) {
		return new cooperatePage(el, data)
	}
	window.mangoFunction = mangoFunction;

	$(document).ready(() => {
		mangoFunction.headerNav();
	})
})(window.mangoFunction || {});